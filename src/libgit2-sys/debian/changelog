rust-libgit2-sys (0.14.1-1) unstable; urgency=medium

  * Package libgit2-sys 0.14.1+1.5.0 from crates.io using debcargo 2.6.0

  [ Blair Noctis ]
  * Team upload.
  * Package libgit2-sys 0.14.0+1.5.0 from crates.io using debcargo 2.6.0

 -- Peter Michael Green <plugwash@debian.org>  Thu, 12 Jan 2023 16:44:43 +0000

rust-libgit2-sys (0.13.2-2) unstable; urgency=medium

  * Team upload.
  * Package libgit2-sys 0.13.2+1.4.2 from crates.io using debcargo 2.5.0
  * Relax libgit2 dependency to allow 1.6

 -- Peter Michael Green <plugwash@debian.org>  Sat, 08 Oct 2022 02:02:01 +0000

rust-libgit2-sys (0.13.2-1) experimental; urgency=medium

  * Team upload.
  * Package libgit2-sys 0.13.2+1.4.2 from crates.io using debcargo 2.5.0

 -- Fabian Grünbichler <f.gruenbichler@proxmox.com>  Tue, 27 Sep 2022 20:35:57 -0400

rust-libgit2-sys (0.12.24-5) unstable; urgency=medium

  * Team upload.
  * Package libgit2-sys 0.12.24+1.3.0 from crates.io using debcargo 2.5.0
  * Upload to unstable.
  * Set collapse_features=true

 -- Peter Michael Green <plugwash@debian.org>  Mon, 02 May 2022 21:11:42 +0000

rust-libgit2-sys (0.12.24-4) experimental; urgency=medium

  * Team upload.
  * Package libgit2-sys 0.12.24+1.3.0 from crates.io using debcargo 2.4.4
  * Drop libgit2 1.1.x patches and bump libgit2 dependencies to 1.3.x
    (Closes: 1009034)

 -- Peter Michael Green <plugwash@debian.org>  Sat, 09 Apr 2022 20:08:50 +0000

rust-libgit2-sys (0.12.24-3) unstable; urgency=medium

  * Package libgit2-sys 0.12.24+1.3.0 from crates.io using debcargo 2.4.4
  * Strictly depend on libgit2 1.1.0 and not later versions.

 -- Ximin Luo <infinity0@debian.org>  Sun, 24 Oct 2021 00:36:16 +0100

rust-libgit2-sys (0.12.24-2) unstable; urgency=medium

  * Package libgit2-sys 0.12.24+1.3.0 from crates.io using debcargo 2.4.4

 -- Ximin Luo <infinity0@debian.org>  Sun, 24 Oct 2021 00:00:06 +0100

rust-libgit2-sys (0.12.24-1) unstable; urgency=medium

  * Package libgit2-sys 0.12.24+1.3.0 from crates.io using debcargo 2.4.4

 -- Ximin Luo <infinity0@debian.org>  Sat, 23 Oct 2021 20:01:36 +0100

rust-libgit2-sys (0.12.13-1) unstable; urgency=medium

  * Team upload.
  * Package libgit2-sys 0.12.13+1.0.1 from crates.io using debcargo 2.4.3
  * Drop the zlib-ng-compat feature, the ftpmasters are rejecting new
    rust feature packages at the moment and the zlib-ng option in
    libz-sys seems to be broken in Debian anyway (see bug 972361)
  * Manually fix debian/tests/control until commit
    293fb88f2156d0db6262349aa4b1c4d3a3b1186a is in a released version of
     debcargo

  [ Ximin Luo ]
  * Package libgit2-sys 0.12.13+1.0.1 from crates.io using debcargo 2.4.3
  * Update patches for new upstream.
  * Depend on libgit2-dev (>= 1)

 -- Peter Michael Green <plugwash@debian.org>  Mon, 07 Dec 2020 05:02:56 +0000

rust-libgit2-sys (0.10.0-1) unstable; urgency=medium

  * Package libgit2-sys 0.10.0 from crates.io using debcargo 2.4.2

 -- Ximin Luo <infinity0@debian.org>  Sat, 18 Apr 2020 16:09:24 +0100

rust-libgit2-sys (0.9.2-2) unstable; urgency=medium

  * Package libgit2-sys 0.9.2 from crates.io using debcargo 2.4.0
  * Make compatible with libgit2 0.28.3

 -- Ximin Luo <infinity0@debian.org>  Thu, 28 Nov 2019 02:55:16 +0000

rust-libgit2-sys (0.9.2-1) unstable; urgency=medium

  * Package libgit2-sys 0.9.2 from crates.io using debcargo 2.4.0

 -- Ximin Luo <infinity0@debian.org>  Thu, 28 Nov 2019 01:58:44 +0000

rust-libgit2-sys (0.9.1-2) unstable; urgency=medium

  * Team upload.
  * Package libgit2-sys 0.9.1 from crates.io using debcargo 2.4.0
  * Source rebuild...

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 14 Nov 2019 22:21:38 +0100

rust-libgit2-sys (0.9.1-1) unstable; urgency=medium

  * Team upload.
  * Package libgit2-sys 0.9.1 from crates.io using debcargo 2.4.0
  * Disable the tests, fails with "file not found" otherwise
  * no-special-snowflake-env.patch removed

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 14 Nov 2019 21:03:27 +0100

rust-libgit2-sys (0.7.11-1) unstable; urgency=medium

  * Package libgit2-sys 0.7.11 from crates.io using debcargo 2.2.10

 -- Ximin Luo <infinity0@debian.org>  Thu, 30 May 2019 21:20:19 -0700

rust-libgit2-sys (0.7.10-1) unstable; urgency=medium

  * Team upload.
  * Package libgit2-sys 0.7.10 from crates.io using debcargo 2.2.9

 -- Matt Kraai <kraai@debian.org>  Sat, 08 Dec 2018 20:17:48 -0800

rust-libgit2-sys (0.7.7-1) unstable; urgency=medium

  * Package libgit2-sys 0.7.7 from crates.io using debcargo 2.2.5

 -- Ximin Luo <infinity0@debian.org>  Fri, 03 Aug 2018 06:41:12 -0700
