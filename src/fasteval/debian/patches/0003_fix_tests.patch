Description: fix tests
 - Recent versions changes `Debug` implementations to use scientific notation on
 numbers that are large or small enough, which breaks the tests.
 - Fix pointer size mismatch in tests
Author: Eric Long <i@hack3r.moe>
Last-Update: 2022-08-21
--- a/tests/parse.rs
+++ b/tests/parse.rs
@@ -1,5 +1,8 @@
 use fasteval::{Error, Slab, Parser};
 
+#[cfg(feature="unsafe-vars")]
+const POINTER_MASK: &str = "????????";
+
 #[test]
 fn basics() {
     let mut slab = Slab::new();
@@ -79,51 +82,51 @@
 
     Parser::new().parse("12.34u", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(0.00001234), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e-5), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("12.34µ", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(0.00001234), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e-5), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("12.34n", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(0.00000001234), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e-8), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("12.34p", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(0.00000000001234), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e-11), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("12.34e56", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(1234000000000000000000000000000000000000000000000000000000.0), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e57), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("12.34e+56", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(1234000000000000000000000000000000000000000000000000000000.0), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e57), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("12.34E56", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(1234000000000000000000000000000000000000000000000000000000.0), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e57), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("12.34E+56", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(1234000000000000000000000000000000000000000000000000000000.0), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e57), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("12.34e-56", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(0.0000000000000000000000000000000000000000000000000000001234), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e-55), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("12.34E-56", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(0.0000000000000000000000000000000000000000000000000000001234), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e-55), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("+12.34E-56", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(0.0000000000000000000000000000000000000000000000000000001234), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(1.234e-55), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("-12.34E-56", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
-"Slab{ exprs:{ 0:Expression { first: EConstant(-0.0000000000000000000000000000000000000000000000000000001234), pairs: [] } }, vals:{}, instrs:{} }");
+"Slab{ exprs:{ 0:Expression { first: EConstant(-1.234e-55), pairs: [] } }, vals:{}, instrs:{} }");
 
     Parser::new().parse("-x", &mut slab.ps).unwrap();
     assert_eq!(format!("{:?}",&slab),
@@ -167,27 +170,15 @@
     fn replace_addrs(mut s:String) -> String {
         let mut start=0;
         loop {
-            match s[start..].find(" 0x") {
-                None => break,
-                Some(i) => {
-                    let v = unsafe { s.as_mut_vec() };
-
-                    start = start+i+3;
-                    loop {
-                        match v.get(start) {
-                            None => break,
-                            Some(&b) => {
-                                if (b'0'<=b && b<=b'9') || (b'a'<=b && b<=b'f') {
-                                    v[start]=b'?';
-                                    start+=1;
-                                } else {
-                                    break;
-                                }
-                            }
-                        }
-                    }
+            if let Some(i) = s[start..].find(" 0x") {
+                start = start+i+3;
+                if let Some(j) = s[start..].find(' ') {
+                    let end = start+j;
+                    s.replace_range(start..end, POINTER_MASK);
                 }
-            };
+            } else {
+                break;
+            }
         }
         s
     }
@@ -203,6 +194,6 @@
 
     Parser::new().parse("ua + ub + 5", &mut slab.ps).unwrap();
     assert_eq!(replace_addrs(format!("{:?}",&slab)),
-"Slab{ exprs:{ 0:Expression { first: EStdFunc(EUnsafeVar { name: \"ua\", ptr: 0x???????????? }), pairs: [ExprPair(EAdd, EStdFunc(EUnsafeVar { name: \"ub\", ptr: 0x???????????? })), ExprPair(EAdd, EConstant(5.0))] } }, vals:{}, instrs:{} }");
+format!("Slab{{ exprs:{{ 0:Expression {{ first: EStdFunc(EUnsafeVar {{ name: \"ua\", ptr: 0x{} }}), pairs: [ExprPair(EAdd, EStdFunc(EUnsafeVar {{ name: \"ub\", ptr: 0x{} }})), ExprPair(EAdd, EConstant(5.0))] }} }}, vals:{{}}, instrs:{{}} }}", POINTER_MASK, POINTER_MASK));
 }
 
--- a/tests/compile.rs
+++ b/tests/compile.rs
@@ -7,6 +7,9 @@
 #[cfg(feature="eval-builtin")]
 use fasteval::compiler::Instruction::IEvalFunc;
 
+#[cfg(feature="unsafe-vars")]
+const POINTER_MASK: &str = "????????";
+
 #[test]
 fn slab_overflow() {
     let mut slab = Slab::with_capacity(2);
@@ -86,27 +89,15 @@
     fn replace_addrs(mut s:String) -> String {
         let mut start=0;
         loop {
-            match s[start..].find(" 0x") {
-                None => break,
-                Some(i) => {
-                    let v = unsafe { s.as_mut_vec() };
-
-                    start = start+i+3;
-                    loop {
-                        match v.get(start) {
-                            None => break,
-                            Some(&b) => {
-                                if (b'0'<=b && b<=b'9') || (b'a'<=b && b<=b'f') {
-                                    v[start]=b'?';
-                                    start+=1;
-                                } else {
-                                    break;
-                                }
-                            }
-                        }
-                    }
+            if let Some(i) = s[start..].find(" 0x") {
+                start = start+i+3;
+                if let Some(j) = s[start..].find(' ') {
+                    let end = start+j;
+                    s.replace_range(start..end, POINTER_MASK);
                 }
-            };
+            } else {
+                break;
+            }
         }
         s
     }
@@ -386,9 +377,9 @@
     #[cfg(feature="unsafe-vars")]
     {
         unsafe_comp_chk("x", "CompileSlab{ instrs:{} }", 1.0);
-        unsafe_comp_chk("x + y", "CompileSlab{ instrs:{ 0:IUnsafeVar { name: \"x\", ptr: 0x???????????? }, 1:IUnsafeVar { name: \"y\", ptr: 0x???????????? } } }", 3.0);
-        unsafe_comp_chk("x() + y", "CompileSlab{ instrs:{ 0:IUnsafeVar { name: \"x\", ptr: 0x???????????? }, 1:IUnsafeVar { name: \"y\", ptr: 0x???????????? } } }", 3.0);
-        unsafe_comp_chk("x(x,y,z) + y", "CompileSlab{ instrs:{ 0:IUnsafeVar { name: \"x\", ptr: 0x???????????? }, 1:IUnsafeVar { name: \"y\", ptr: 0x???????????? } } }", 3.0);
+        unsafe_comp_chk("x + y", &format!("CompileSlab{{ instrs:{{ 0:IUnsafeVar {{ name: \"x\", ptr: 0x{} }}, 1:IUnsafeVar {{ name: \"y\", ptr: 0x{} }} }} }}", POINTER_MASK, POINTER_MASK), 3.0);
+        unsafe_comp_chk("x() + y", &format!("CompileSlab{{ instrs:{{ 0:IUnsafeVar {{ name: \"x\", ptr: 0x{} }}, 1:IUnsafeVar {{ name: \"y\", ptr: 0x{} }} }} }}", POINTER_MASK, POINTER_MASK), 3.0);
+        unsafe_comp_chk("x(x,y,z) + y", &format!("CompileSlab{{ instrs:{{ 0:IUnsafeVar {{ name: \"x\", ptr: 0x{} }}, 1:IUnsafeVar {{ name: \"y\", ptr: 0x{} }} }} }}", POINTER_MASK, POINTER_MASK), 3.0);
     }
 
     // IFunc
