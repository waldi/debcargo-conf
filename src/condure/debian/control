Source: rust-condure
Section: net
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native,
 rustc:native,
 libstd-rust-dev,
 librust-arrayvec-0.7+default-dev,
 librust-base64-0.21+default-dev,
 librust-clap-4+cargo-dev (>= 4.1.13-~~),
 librust-clap-4+default-dev (>= 4.1.13-~~),
 librust-clap-4+string-dev (>= 4.1.13-~~),
 librust-clap-4+wrap-help-dev (>= 4.1.13-~~),
 librust-httparse-1+default-dev (>= 1.7-~~),
 librust-ipnet-2+default-dev,
 librust-libc-0.2+default-dev,
 librust-log-0.4+default-dev,
 librust-miniz-oxide-0.7+default-dev,
 librust-mio-0.8+default-dev,
 librust-mio-0.8+net-dev,
 librust-mio-0.8+os-ext-dev,
 librust-mio-0.8+os-poll-dev,
 librust-openssl-0.10+default-dev,
 librust-paste-1+default-dev,
 librust-sha1-0.10+default-dev,
 librust-signal-hook-0.3+default-dev,
 librust-slab-0.4+default-dev,
 librust-socket2-0.4+default-dev,
 librust-thiserror-1+default-dev,
 librust-time-0.3+default-dev,
 librust-time-0.3+formatting-dev,
 librust-time-0.3+local-offset-dev,
 librust-time-0.3+macros-dev,
 librust-url-2+default-dev (>= 2.3-~~),
 librust-zmq-0.9+default-dev,
 help2man
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Jan Niehusmann <jan@debian.org>
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/condure]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/condure
X-Cargo-Crate: condure
Rules-Requires-Root: no

Package: condure
Architecture: any
Multi-Arch: allowed
Section: net
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
XB-X-Cargo-Built-Using: ${cargo:X-Cargo-Built-Using}
Description: HTTP/WebSocket connection manager
 Condure is a service that manages network connections on behalf of server
 applications, in order to allow controlling the connections from multiple
 processes. Applications communicate with Condure over ZeroMQ.
 .
 Condure can only manage connections for protocols it knows
 about. Currently this is HTTP/1 and WebSockets. See Supported protocols.
 .
 The project was inspired by Mongrel2.
