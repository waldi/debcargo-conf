Source: rust-md-5
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-digest-0.10+default-dev (>= 0.10.3-~~) <!nocheck>,
 librust-digest-0.10+std-dev (>= 0.10.3-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 kpcyrd <git@rxv.cc>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/md-5]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/md-5
Rules-Requires-Root: no

Package: librust-md-5-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-digest-0.10+default-dev (>= 0.10.3-~~)
Recommends:
 librust-md-5+std-dev (= ${binary:Version})
Suggests:
 librust-md-5+md5-asm-dev (= ${binary:Version})
Provides:
 librust-md-5-0-dev (= ${binary:Version}),
 librust-md-5-0.10-dev (= ${binary:Version}),
 librust-md-5-0.10.1-dev (= ${binary:Version})
Description: MD5 hash function - Rust source code
 Rust crate md-5

Package: librust-md-5+md5-asm-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-md-5-dev (= ${binary:Version}),
 librust-md5-asm-0.5+default-dev
Provides:
 librust-md-5+asm-dev (= ${binary:Version}),
 librust-md-5-0+md5-asm-dev (= ${binary:Version}),
 librust-md-5-0+asm-dev (= ${binary:Version}),
 librust-md-5-0.10+md5-asm-dev (= ${binary:Version}),
 librust-md-5-0.10+asm-dev (= ${binary:Version}),
 librust-md-5-0.10.1+md5-asm-dev (= ${binary:Version}),
 librust-md-5-0.10.1+asm-dev (= ${binary:Version})
Description: MD5 hash function - feature "md5-asm" and 1 more
 This metapackage enables feature "md5-asm" for the Rust md-5 crate, by pulling
 in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "asm" feature.

Package: librust-md-5+std-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-md-5-dev (= ${binary:Version}),
 librust-digest-0.10+std-dev (>= 0.10.3-~~)
Provides:
 librust-md-5+default-dev (= ${binary:Version}),
 librust-md-5-0+std-dev (= ${binary:Version}),
 librust-md-5-0+default-dev (= ${binary:Version}),
 librust-md-5-0.10+std-dev (= ${binary:Version}),
 librust-md-5-0.10+default-dev (= ${binary:Version}),
 librust-md-5-0.10.1+std-dev (= ${binary:Version}),
 librust-md-5-0.10.1+default-dev (= ${binary:Version})
Description: MD5 hash function - feature "std" and 1 more
 This metapackage enables feature "std" for the Rust md-5 crate, by pulling in
 any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "default" feature.
